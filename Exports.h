#if !defined(AFX_EXPORTS_H)
#define AFX_EXPORTS_H

#define __BUILD

#ifdef __BUILD
#define LOGSCALE_DLL_BUILD extern __declspec(dllexport)
#else
#define LOGSCALE_DLL_BUILD extern __declspec(dllimport)
#endif

#include "stdafx.h"


extern "C"
{
	LOGSCALE_DLL_BUILD void getDLLVolFuncs(vecFuncDesc &v);

	LOGSCALE_DLL_BUILD void calculateDLLVolLogs(CLogs &rec,vecExtraVolFuncID &vec,CVecUserVolTables &vec1,double taper_ib,int use_southern_doyle,int measure_mode,int deduction,double trim);

}


#endif