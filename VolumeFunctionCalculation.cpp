#include "stdafx.h"
#include "volumefunctioncalculation.h"

////////////////////////////////////////////////////////////////////////////
// Baseclass for handling calculation of volumes
CCalculationBase::CCalculationBase()	
{

}

////////////////////////////////////////////////////////////////////////////
// Derived class from CCalculationBase, handle calculation of
// preset volumefunctions
CCalculationPreset::CCalculationPreset()
: CCalculationBase()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PRIVATE METHODS

//***************************************************************************************
// MISC. METHODS
void CCalculationPreset::tokenizeString(LPCTSTR str,TCHAR c,CStringArray& items)
{
	TCHAR szLine[255];
	CString sToken = L"";
	int nTokenCnt = 0;
	
	items.RemoveAll();
	memset(szLine,0,255);
	_tcscpy(szLine,str);
	// Count tokens in string
	for (TCHAR *p = szLine;p < _tcslen(szLine)+szLine;p++)
	{
		if (*p == c)
			nTokenCnt++;
	}
	for (int i = 0;i < nTokenCnt;i++)
	{
		AfxExtractSubString(sToken,szLine,i,c);
		items.Add(sToken);
	}

}


double CCalculationPreset::getScalingDiam(double d1,double d2,enumScalingDiamTypes scaling_type)
{
	double dAbs1 = 0.0;
	double dAbs2 = 0.0;
	double dAbsRet = 0.0;
	double dDiam = 0.0;

	if (scaling_type == NO_FRACTIONS)	// Average value, remove fractions
	{
		modf(d1,&dAbs1);
		modf(d2,&dAbs2);

		if (dAbs1 > 0.0 && dAbs2 > 0.0)
			return (abs((dAbs1+dAbs2)/2.0));
		if (dAbs1 > 0.0 && dAbs2 == 0.0)
			return (dAbs1);
		if (dAbs1 == 0.0 && dAbs2 > 0.0)
			return (dAbs2);
	}
	else 	if (scaling_type == ROUND_NO_FRACTIONS)	// Round value Average value, remove fractions
	{
		CString sRoundD1 = L"";
		CString sRoundD2 = L"";
		sRoundD1.Format(L"%.0f",d1);
		sRoundD2.Format(L"%.0f",d2);
		dAbs1 = _tstof(sRoundD1);
		dAbs2 = _tstof(sRoundD2);
		if (dAbs1 > 0.0 && dAbs2 > 0.0)
			return (abs((dAbs1+dAbs2)/2.0));
		if (dAbs1 > 0.0 && dAbs2 == 0.0)
			return (dAbs1);
		if (dAbs1 == 0.0 && dAbs2 > 0.0)
			return (dAbs2);
	}
	else 	if (scaling_type == SIMPLE)	// Round value Average value, remove fractions
	{
		if (d1 > 0.0 && d2 > 0.0)
			return (d1+d2)/2.0;
		if (d1 > 0.0 && d2 == 0.0)
			return d1;
		if (d1 == 0.0 && d2 > 0.0)
			return d2;
	}
	else 	if (scaling_type == HUBER_TR)	// Round value Average value, remove fractions
	{
		modf(d1,&dAbs1);
		modf(d2,&dAbs2);
		if (d1 <= d2)
		{
			dAbs1 -= 1.0;
			dAbs2 += 1.0;
			dDiam = (dAbs1+dAbs2)/2.0;
			modf(dDiam,&dAbsRet);
			return dAbsRet+1.0;
		}
		else
		{
			dAbs1 += 1.0;
			dAbs2 -= 1.0;
			dDiam = (dAbs1+dAbs2)/2.0;
			modf(dDiam,&dAbsRet);
			return dAbsRet+1.0;
		}
	}
	else 	if (scaling_type == GB_SCALE)	// Round value Average value, remove fractions
	{
		char s[10];
		double fFrac1 = 0.0,fFrac2 = 0.0;
		int nDS = 0,nPS = 0;
		fFrac1 = modf(d1,&dAbs1);
		sprintf(s,"%.0f",dAbs1);
		nDS = atoi(s);

		fFrac2 = modf(d2,&dAbs2);
		sprintf(s,"%.0f",dAbs2);
		nPS = atoi(s);
		if ((nDS % 2) == 1)
		{
			/*if (d1 <= d2 || d2 <= d1)
			{
				if (fFrac1 >= 0.0 && fFrac1 < 0.6)
				{
					dAbs1 -= 1.0;
				}
				else if (fFrac1 >= 0.6 && fFrac1 <= 0.9)
				{
					dAbs1 += 1.0;
				}
			}*/
			dAbs1 += 1.0;
		}
		if ((nPS % 2) == 1)
		{
			/*if (d1 <= d2 || d2 <= d1)
			{
				if (fFrac2 >= 0.0 && fFrac2 < 0.6)
				{
					dAbs2 -= 1.0;
				}
				else if (fFrac2 >= 0.6 && fFrac2 <= 0.9)
				{
					dAbs2 += 1.0;
				}
			}*/
			dAbs2 += 1.0;
		}


		if (d1 > 0.0 && d2 > 0.0)
		{
			double val = 0.0,dabs = 0.0;
			int absv = 0;
			absv = abs(dAbs1 - dAbs2);
			val = (absv/4.0);
			modf(val,&dabs);
			return min(dAbs1,dAbs2) + 2.0*dabs;
		}
		else if (d1 > 0.0 && d2 == 0.0)
				return dAbs1;
		else if (d1 == 0.0 && d2 > 0.0)
				return dAbs2;
	}

	return 0.0;
}

double CCalculationPreset::getAbs(double v)
{
	double fValue = 0.0;
	modf(v,&fValue);
	return fValue;
}

double CCalculationPreset::getHuberTRLength(double len)
{
	char s[32];
	double fIntPart = 0.0,fFracPart = 0.0;

	fFracPart = modf(len,&fIntPart);
	sprintf(s,"%.1f",fFracPart);
	fFracPart = atof(s);
	//--------------------------------------------------
	// If fractional part between .95 to .99, 
	// round to on decimal will result in value eq. 1.0
	if (fFracPart == 1.0)
		fFracPart -= 0.1;	// Set to .9 instead
	//--------------------------------------------------

	if (fFracPart >= 0.1 && fFracPart <= 0.5)
		return fIntPart;
	else if ((fFracPart >= 0.6 && fFracPart <= 0.9))
		return fIntPart+0.5;
	else if (fFracPart == 0.0)
		return fIntPart-0.5;

	return 0.0;
}

double CCalculationPreset::getGBScaleLength(double len)
{
	char s[10];
	double fFracPart = 0.0,fIntPart = 0.0;
	int nFrac = 0;

	fFracPart = modf(len,&fIntPart);
	sprintf(s,"%.0f",fFracPart*100.0);
	nFrac = atoi(s)/10;
	if ((nFrac % 2) == 1)
	{
		nFrac -= 1;
	}
	return fIntPart + nFrac/10.0;
}

//***************************************************************************************
// Methods for JAS (Japanese Agriculture Standard)
double CCalculationPreset::addToJAS(double dS,double dL)
{
	double i = 0;
	double dDiff = abs(dS - dL),fIntFac = 0.0,fCalcFac  = 0.0,fReturn = 0.0;
	
	if ((dS > 14.0 && dS < 40.0) || (dL > 14.0 && dL < 40.0))
	{

		modf(dDiff/6.0,&fIntFac);
		for (i = 0;i < fIntFac;i++)
		{
			fCalcFac += 2.0;
		}

	}
	else if ((dS >= 40.0) || (dL >= 40.0))
	{
		modf(dDiff/8.0,&fIntFac);
		for (i = 0;i < fIntFac;i++)
		{
			fCalcFac += 2.0;
		}
	}

	return fCalcFac;
}


double CCalculationPreset::checkDiameter(enumConvertType convert_type,double dS,double dL)
{
	char s[10];
	int nDS = 0,nDL = 0;
	double fFractPartS = 0.0,fIntPartS = 0.0;
	double fFractPartL = 0.0,fIntPartL = 0.0;

	if (convert_type == CONV_JAS)
	{
		if (dS >= 14.0 && dL >= 14.0)
		{
			if (dS < dL)
			{
				modf(dS,&fIntPartS);
				sprintf(s,"%.0f",fIntPartS);
				nDS = atoi(s);
				if ((nDS % 2) == 1)
					nDS -= 1;

				modf(dL,&fIntPartL);
				sprintf(s,"%.0f",fIntPartL);
				nDL = atoi(s);
				if ((nDL % 2) == 1)
					nDL -= 1;

				return nDS*1.0 + addToJAS(nDS*1.0,nDL*1.0);
			}
			else if (dS > dL || dS == dL)
			{
				modf(dS,&fIntPartS);
				sprintf(s,"%.0f",fIntPartS);
				nDS = atoi(s);
				if ((nDS % 2) == 1)
					nDS -= 1;

				modf(dL,&fIntPartL);
				sprintf(s,"%.0f",fIntPartL);
				nDL = atoi(s);
				if ((nDL % 2) == 1)
					nDL -= 1;
	
				return nDL*1.0 + addToJAS(nDS*1.0,nDL*1.0);
			}
			
		}
		else if (dS < 14.0 && (dL > 14.0 || dL == 0.0))
		{
			modf(dS,&fIntPartS);
			return fIntPartS;
			/*
			sprintf(s,"%.0f",fIntPartS);
			nDS = atoi(s);
			if ((nDS % 2) == 1)
				nDS -= 1;

			return nDS*1.0;
			*/
		}
		else if (dL < 14.0 && (dS > 14.0 || dS == 0.0))
		{
			modf(dL,&fIntPartL);
			return fIntPartL;
			/*
			sprintf(s,"%.0f",fIntPartL);
			nDS = atoi(s);
			if ((nDS % 2) == 1)
				nDS -= 1;
			
			return nDS*1.0;
			*/
		}
		else if (dS < 14.0 && dL < 14.0)
		{
			if (dS > 0.0 && (dL == 0.0 || dS < dL))
			{
				modf(dS,&fIntPartS);
				return fIntPartS;
				/*
				sprintf(s,"%.0f",fIntPartS);
				nDS = atoi(s);
				if ((nDS % 2) == 1)
					nDS -= 1;
				return nDS*1.0;
				*/
			}
			else if (dL > 0.0 && (dS == 0.0 || dL < dS))
			{
				modf(dL,&fIntPartL);
				return fIntPartL;
				/*
				sprintf(s,"%.0f",fIntPartL);
				nDS = atoi(s);
				if ((nDS % 2) == 1)
					nDS -= 1;
				return nDS*1.0;
				*/
			}
			else if (dL == dS)
			{
				modf(dL,&fIntPartL);
				return fIntPartL;
				/*
				sprintf(s,"%.0f",fIntPartL);
				nDS = atoi(s);
				if ((nDS % 2) == 1)
					nDS -= 1;
				return nDS*1.0;
				*/
			}
		}

	}
	else if (convert_type == CONV_HIRAGOKU)
	{
		modf(dS,&fIntPartS);
		return fIntPartS;
	}
	else if (convert_type == CONV_SSL)
	{
		modf(dS,&fIntPartS);
		modf(dL,&fIntPartL);
		return (fIntPartS + fIntPartL)/2.0;
	}

	return 0.0;
}
// Method used in West-Side Scribner
void CCalculationPreset::segment(double length,vecSegmentValues& values)
{
	char s[10];
	double fInt = 0.0;
	double fFrac = 0.0,fFrac1 = 0.0,fFracCnt = 0.0,fAddToDiam[3];
	int dInt = 0,nCnt = 0;
	double fLength[4];

	values.clear();

	for (int i = 0;i < 4;i++)
		fLength[i] = 0.0;
	for (int i = 0;i < 3;i++)
		fAddToDiam[i] = 0.0;
	// No segmentation
	if (length < 41.0) 
		values.push_back(segment_values(length,0.0));

	// Two segemnts
	else if (length >= 41.0 && length < 81)
	{
		fFrac = modf(length/2.0,&fInt);
		sprintf(s,"%.0f",fInt);
		dInt = atoi(s);
		if ((dInt % 2) != 0)
			dInt -= 1;
		sprintf(s,"%.0f",((fInt-dInt)+fFrac)*2.0);
		fFrac1 = atof(s);
		//printf("Length %.1f     Int %.1f  %d    Frac %.1f  Frac2 %.1f\n",length,fInt,dInt,fFrac,fFrac1);

		for (int i = 0;i < 2;i++)
		{
			if (fFrac1 > 0.0)
			{
				fFracCnt = (fFrac1 >= 2.0 ? 2.0 : 1.0);
				fFrac1 -= 2.0;
			}
			else
				fFracCnt = 0.0;
			fLength[i] = dInt+fFracCnt;
		}
		if (length < 58.0)
			fAddToDiam[0] = 2.0;
		else if (length >= 58.0 && length < 78.0)
			fAddToDiam[0] = 3.0;
		else if (length >= 78.0)
			fAddToDiam[0] = 4.0;

		values.push_back(segment_values(fLength[0],(fLength[0] < fLength[1] ? fAddToDiam[0] : 0.0)));
		values.push_back(segment_values(fLength[1],(fLength[0] < fLength[1] ? 0.0 : fAddToDiam[0])));
	}
	// Three segemnts
	else if (length >= 81.0  && length < 121.0)
	{
		fFrac = modf(length/3.0,&fInt);
		sprintf(s,"%.0f",fInt);
		dInt = atoi(s);
		if ((dInt % 2) != 0)
			dInt -= 1;
		sprintf(s,"%.0f",((fInt-dInt)+fFrac)*3.0);
		fFrac1 = atof(s);
		//printf("Length %.1f     Int %.1f  %d    Frac %.1f  Frac2 %.1f\n",length,fInt,dInt,fFrac,fFrac1);

		for (int i = 0;i < 3;i++)
		{
			if (fFrac1 > 0.0)
			{
				fFracCnt = (fFrac1 >= 2.0 ? 2.0 : 1.0);
				fFrac1 -= 2.0;
			}
			else
				fFracCnt = 0.0;
			fLength[i] = dInt+fFracCnt;
		}
		
		if (length >= 86.0 && length <= 87.9)
		{
			fAddToDiam[0] = 3.0;
			fAddToDiam[1] = 2.0;
		}
		else if (length >= 88.0 && length <= 103.9)
		{
			fAddToDiam[0] = 3.0;
			fAddToDiam[1] = 3.0;
		}
		else if (length >= 104.0 && length <= 115.9)
		{
			fAddToDiam[0] = 3.0;
			fAddToDiam[1] = 4.0;
		}
		else if (length >= 116.0 && length <= 117.9)
		{
			fAddToDiam[0] = 4.0;
			fAddToDiam[1] = 3.0;
		}
		else if (length >= 118.0 && length <= 120.9)
		{
			fAddToDiam[0] = 4.0;
			fAddToDiam[1] = 4.0;
		}
		else
		{
			fAddToDiam[0] = 2.0;
			fAddToDiam[1] = 3.0;
		}

		values.push_back(segment_values(fLength[0],0.0));
		values.push_back(segment_values(fLength[1],fAddToDiam[0]));
		values.push_back(segment_values(fLength[2],fAddToDiam[1]));
	}
	// Four segemnts
	else if (length >= 121.0)
	{
		fFrac = modf(length/4.0,&fInt);
		sprintf(s,"%.0f",fInt);
		dInt = atoi(s);
		if ((dInt % 2) != 0)
			dInt -= 1;
		sprintf(s,"%.0f",((fInt-dInt)+fFrac)*4.0);
		fFrac1 = atof(s);
		for (int i = 0;i < 4;i++)
		{
			if (fFrac1 > 0.0)
			{
				fFracCnt = (fFrac1 >= 2.0 ? 2.0 : 1.0);
				fFrac1 -= 2.0;
			}
			else
				fFracCnt = 0.0;
			fLength[i] = dInt+fFracCnt;
		}
		if (length >= 121.0 && length <= 131.9)
		{
			fAddToDiam[0] = 3.0;
			fAddToDiam[1] = 3.0;
			fAddToDiam[2] = 3.0;
		}
		else if (length >= 132.0 && length <= 137.9)
		{
			fAddToDiam[0] = 3.0;
			fAddToDiam[1] = 3.0;
			fAddToDiam[2] = 4.0;
		}
		else
		{
			fAddToDiam[0] = 3.0;
			fAddToDiam[1] = 4.0;
			fAddToDiam[2] = 3.0;
		}

		values.push_back(segment_values(fLength[0],0.0));
		values.push_back(segment_values(fLength[1],fAddToDiam[0]));
		values.push_back(segment_values(fLength[2],fAddToDiam[1]));
		values.push_back(segment_values(fLength[3],fAddToDiam[2]));

	}
}

//***************************************************************************************
// VOLUME CALCULATION
//double CCalculationPreset::GetLogVolume(BYTE nVolumeIndex, double dSD1,double dSD2, double dLen, double dTaperIB, double dBarkRatio,
//				    WORD wUseSouthernDoyle, double dLeSD1,double dLeSD2, int nMeasureSys)
double CCalculationPreset::GetLogVolume(int nVolumeIndex,CLogs &rec,double dTaperIB,WORD wUseSouthernDoyle,int nMeasureSys)
{
	double dVolume = 0;
	double dSD1 = rec.getDIBTop();
	double dSD2 = rec.getDIBTop2();
	double dLen = rec.getLengthCalc();
	double dLeSD1 = rec.getDIBRoot();
	double dLeSD2 = rec.getDIBRoot2();
	double dBarkRatio = rec.getBark();

	if((dSD1 > 0.0 || dSD2 > 0.0)&&(dLen > 0.0))
	{
		if (nMeasureSys > 0 && (nVolumeIndex != ID_JAS /* JAS*/ && nVolumeIndex != ID_HUBER && nVolumeIndex != ID_HUBER_TR && nVolumeIndex != ID_GB_SCALE) )
		{
			dSD1 *= MM2IN;
			dSD2 *= MM2IN;	// Can be 0.0
			dLeSD1 *= MM2IN;
			dLeSD2 *= MM2IN;

			if(nMeasureSys == 1)
			{
				dLen *= M2FT;
			}
			else
			{
				dLen *= M2FT;
			}
		}
		if (nMeasureSys == 0 && (nVolumeIndex == ID_JAS /* JAS*/ || nVolumeIndex == ID_HUBER || nVolumeIndex == ID_HUBER_TR || nVolumeIndex == ID_GB_SCALE))
		{
			dSD1 *= IN2MM;
			dSD2 *= IN2MM;	// Can be 0.0
			dLeSD1 *= IN2MM;
			dLeSD2 *= IN2MM;

			if(nMeasureSys == 1)
			{
				dLen *= FT2M;
			}
			else
			{
				dLen *= FT2M;
			}
		}

		switch(nVolumeIndex)
		{
			case ID_INT14:
				dVolume = GetInt8(getScalingDiam(dSD1,dSD2,SIMPLE), dLen, dTaperIB, getScalingDiam(dLeSD1,dLeSD2,SIMPLE)) * 0.905;
				break;
			case ID_INT18:
				dVolume = GetInt8(getScalingDiam(dSD1,dSD2,SIMPLE), dLen, dTaperIB, getScalingDiam(dLeSD1,dLeSD2,SIMPLE));
				break;
			case ID_DOYLE:
				dVolume = GetDoyle(getScalingDiam(dSD1,dSD2,SIMPLE), dLen, wUseSouthernDoyle);
				break;
			case ID_BRUCE_SHUM:
				// Bruce/Shumacher
				dVolume = GetBruceShum(getScalingDiam(dSD1,dSD2,SIMPLE), getAbs(dLen));
				break;
			case ID_SCRIB_W:
				// Weast-side scribner
				dVolume = GetScribnerW(getScalingDiam(dSD1,dSD2,SIMPLE), getAbs(dLen));
				break;
			case ID_JAS:
				dVolume = GetJAS(checkDiameter(CONV_JAS,dSD1/10.0,dSD2/10.0), dLen)/1000.0;
				break;
			case ID_HUBER:
				dVolume = GetHuber(getScalingDiam(dSD1/10.0,dSD2/10.0,SIMPLE)/100.0, dLen); ///1000.0;
				break;
			case ID_HUBER_TR:
				dVolume = GetHuberTR(getScalingDiam(dSD1/10.0,dSD2/10.0,HUBER_TR)/100.0, getHuberTRLength(dLen)); ///1000.0;
				break;
			case ID_GB_SCALE:
				dVolume = GetGBScale(getScalingDiam(dSD1/10.0,dSD2/10.0,GB_SCALE), getGBScaleLength(dLen)); ///1000.0;
				break;
			case ID_CFIB:
				dVolume = GetCfib(getScalingDiam(dSD1,dSD2,SIMPLE), dLen, dTaperIB, getScalingDiam(dLeSD1,dLeSD2,SIMPLE));
				break;
			case ID_CFIB_100:
				dVolume = GetCfib(getScalingDiam(dSD1,dSD2,SIMPLE), dLen, dTaperIB, getScalingDiam(dLeSD1,dLeSD2,SIMPLE))/100.00;
				break;
			case ID_CFOB:
				{
					if (dBarkRatio == 1.0)
						dBarkRatio = 0.0;
					dVolume = GetCfob(getScalingDiam(dSD1,dSD2,SIMPLE)+dBarkRatio, dLen, dTaperIB, dBarkRatio, getScalingDiam(dLeSD1,dLeSD2,SIMPLE));
				}
				break;
			default:
				//---------------------------------------------------------------
				// We'll also handle userdefined volumefunctions
				if (m_vecUserVolTables.size() > 0)
				{
					for (UINT i2 = 0;i2 < m_vecUserVolTables.size();i2++)
					{
						if (m_vecUserVolTables[i2].getFuncID() == nVolumeIndex)
						{
							switch (m_vecUserVolTables[i2].getTableType())
							{
								// First type of volumetable
								case 0 :
									// Both are set Inch or both are set Metric
									if ((m_vecUserVolTables[i2].getMeasuringMode() == 0 && nMeasureSys == 0) || (m_vecUserVolTables[i2].getMeasuringMode() == 1 && nMeasureSys == 1))
									{
										dVolume = getUserVolFunc1(rec.getSpcID(),m_vecUserVolTables[i2].getVolTable(),rec.getDIBTop(),rec.getDIBTop2(),rec.getLengthCalc());
									}
									// Ticket is Inch
									// Usertable is metric
									else if (m_vecUserVolTables[i2].getMeasuringMode() == 1 && nMeasureSys == 0)
									{
										dVolume = getUserVolFunc1(rec.getSpcID(),m_vecUserVolTables[i2].getVolTable(),rec.getDIBTop()*IN2MM,rec.getDIBTop2()*IN2MM,rec.getLengthCalc()*FT2M);
									}
									// Ticket is metric
									// Usertable is Inch
									else if (m_vecUserVolTables[i2].getMeasuringMode() == 0 && nMeasureSys == 1)
									{
										dVolume = getUserVolFunc1(rec.getSpcID(),m_vecUserVolTables[i2].getVolTable(),rec.getDIBTop()*MM2IN,rec.getDIBTop2()*MM2IN,rec.getLengthCalc()*M2FT);
									}
									break;
							}
						}
					}
				}
				break;
		}
	}

	if (dVolume > 0.0)
		return dVolume;
	else
		return 0.0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////
// VOLUME FUNCTIONS

// Doyle
double CCalculationPreset::GetDoyle(double dSD, double dLen, WORD wUseSouthernDoyle)
{
	if((dSD < 8.0)&&(wUseSouthernDoyle != 0))
  {
		return dLen;
  }
  else
  {
		return ((dSD - 4.0)/4.0)*((dSD - 4.0)/4.0)*(dLen);
  }
}

// Knauf
double CCalculationPreset::GetKnauf(double dSD, double dLen)
{
	double fValue1 = (pow(dSD,2)-dSD*3.0)/10.0;
	double fValue2 = dLen/2.0;
	return fValue1 * fValue2;
}
// Bruce and Schumacher
double CCalculationPreset::GetBruceShum(double dSD, double dLen)
{
	return ((0.79*dSD - 2.0)*dSD - 4.0)*(dLen)/16.0;
}

// West-side
double CCalculationPreset::GetScribnerW(double dSD, double dLen)
{
	double fValue = 0.0;
	double fVolumeCalculted = 0.0;
	double fDiam = 0.0,fCalcDiam = 0.0,fTmp = 0.0;
	double fPrevLength = 0.0;
	vecSegmentValues val;
	segment(dLen,val);
	modf(dSD,&fDiam);
	fCalcDiam = fDiam;
	for (UINT i = 0;i < val.size();i++)
	{
		segment_values v = val[i];
		fCalcDiam = (fPrevLength > 0.0 ? dSD + fPrevLength/10.0 : fDiam);
		modf(fCalcDiam,&fDiam);
		fCalcDiam = fDiam;
		fValue = (((pow(fCalcDiam,2)-fCalcDiam*3.0)/10.0*v.getLength()/2.0)/10.0)*10.0;
		modf(fValue/10.0,&fTmp);
		fVolumeCalculted += fTmp*10.0;
		fPrevLength += v.getLength();
	}
	return fVolumeCalculted;
}
// Bruce and Schumacher; maybe for East-side
double CCalculationPreset::GetScribnerE(double dSD, double dLen)
{
	return ((0.79*dSD - 2.0)*dSD - 4.0)*(dLen)/16.0;
}

// JAS (Japanese Agriculture Standard)
double CCalculationPreset::GetJAS(double dSD, double dLen)
{
	double fValue = 0.0,fIntL = 0.0;
	if (dLen < 6.0)
		return (dSD*dSD*dLen)/10.0;	// In dm3
	else if (dLen >= 6.0)
	{
		modf(dLen,&fIntL);
		fValue = dSD + (fIntL - 4.0)/2.0;

		return fValue*fValue*dLen/10.0;	// In dm3
	}

	return 0.0;
}

// Huber
double CCalculationPreset::GetHuber(double dSD, double dLen)
{
	return dSD*dSD*dLen*0.7854;
}

// Huber Turkey
double CCalculationPreset::GetHuberTR(double dSD, double dLen)
{
	return dSD*dSD*dLen*0.7854;
}

// GB Scale (Gua Biao)
double CCalculationPreset::GetGBScale(double dSD, double dLen)
{
	double fPart = 0.0;
	if (dSD >= 4.0 && dSD < 14.0)
		return (0.7854*dLen*((dSD+0.45*dLen+0.2)*(dSD+0.45*dLen+0.2)))/10000.0;
	else if (dSD >= 14.0)
	{
		fPart =	(dSD+0.5*dLen+0.005*dLen*dLen+0.000125*dLen*((14-dLen)*(14-dLen))*(dSD-10.0));
		return (0.7854*dLen*fPart*fPart)/10000.0;
	}
	else
		return 0.0;
}


double CCalculationPreset::GetScrib16FootSegm(double dSD,double dTaperIB)
{
	// As set in EXCEL sheet sent from Brian M.
	// (DIB^2-8*3)/10*7/2+((DIB+8*TaperIB)^2-16*3)/10*16/2+((DIB+24*TaperIB)^2-16*3)/10*16/2

	// (DIB*DIB-24)/10*3.5
	double fArg1 = (dSD*dSD-24.0)/10.0*3.5;
	// ((((DIB+8*TaperIB)*(DIB+8*TaperIB))-48)/10.0*8.0)
	double fArg2 = (((dSD+8.0*dTaperIB)*(dSD+8.0*dTaperIB))-48.0)/10.0*8.0;
	// ((((DIB+24*TaperIB)*(DIB+24*TaperIB))-48)/10.0*8.0)
	double fArg3 = (((dSD+24.0*dTaperIB)*(dSD+24.0*dTaperIB))-48.0)/10.0*8.0;
	
	return fArg1+fArg2+fArg3;
}

double CCalculationPreset::GetDoyle16FootSegm(double dSD,double dTaperIB)
{
	// As set in EXCEL sheet sent from Brian M.
	// ((DIB-4)/4)^2*8+((DIB+(TaperIB*8)-4)/4)^2*16+((DIB+(TaperIB*24)-4)/4)^2*16

	// ((DIB-4)/4)^2*8
	double fArg1 = (((dSD-4.0)/4.0)*((dSD-4.0)/4.0))*8.0;
	// ((DIB+(TaperIB*8)-4)/4)^2*16
	double fArg2 = ((dSD+(dTaperIB*8.0)-4.0)/4.0)*((dSD+(dTaperIB*8.0)-4.0)/4.0)*16.0;
	// ((DIB+(TaperIB*24)-4)/4)^2*16
	double fArg3 = ((dSD+(dTaperIB*24.0)-4.0)/4.0)*((dSD+(dTaperIB*24.0)-4.0)/4.0)*16.0;

	return fArg1+fArg2+fArg3;
}

double CCalculationPreset::GetInt8(double dSD, double dLen, double dTaperIB, double dLeSD)
{
	double dSum;
  double dLenS;
  double dTaperIB4;
  double dCumLen = 4;

	if((dLeSD > 0)&&(dLen > 0))
	{
		dTaperIB = (dLeSD - dSD)/dLen;
	}

  dTaperIB4 = 4.0*dTaperIB;
  dSum = 0.0;
  while(dCumLen <= dLen)
  {
      dSum += (0.22*dSD - .71)*dSD;
      dSD += dTaperIB4;
      dCumLen += 4;
        
  }
  dLenS =  dLen - (dCumLen- 4);
  if(dLenS > 0)
  {
      dSD -= dTaperIB4;
      dSD += (double)dLenS*dTaperIB;
      dSum += ((0.22*dSD - .71)*dSD)*(dLenS)/4.0;
  }
  return dSum;
}

double CCalculationPreset::GetCfib(double dSDIB, double dLen, double dTaperIB, double dLeSD)
{
	double dLeSDIB;
  double dSEndDia;
  double dLEndDia;
	double dSECSA;
	double dLECSA;
	double dX = 0;
	double dBoltLen = 4;
	double dVolume = 0;

	dLeSDIB = dLeSD;

	if((dLeSDIB > 0)&&(dLen > 0))
	{
		dTaperIB = (dLeSDIB - dSDIB)/dLen;
	}
	 
  dSEndDia = dSDIB;

	dSECSA = 0.002727*dSEndDia*dSEndDia;

	dX = dBoltLen;

	while(dX <= dLen)
	{
		dLEndDia = dSEndDia + dTaperIB*dBoltLen;
		dLECSA = 0.002727*dLEndDia*dLEndDia;

		dVolume += (dSECSA + dLECSA)*dBoltLen;

		dSEndDia = dLEndDia;
		dSECSA = dLECSA;

		dX += dBoltLen;
	}
	dX -= dBoltLen;

	if(dX < dLen)
	{
			dLEndDia = dSEndDia + dTaperIB*(dLen - dX);
			dLECSA = 0.002727*dLEndDia*dLEndDia;

			dVolume += (dSECSA + dLECSA)*(dLen - dX);
	}

  return dVolume;
}

double CCalculationPreset::GetCfob(double dSDOB, double dLen, double dTaperIB, double dBarkRatio, double dLeSD)
{
	double dBRatio = 0.0;
  double dSDIB;
  double dLeSDIB;
  double dSEndDia;
  double dLEndDia;
	double dSECSA;
	double dLECSA;
	double dTaperOB;
	double dX = 0;
	double dBoltLen = 4;
	double dVolume = 0;

	dSDIB = dSDOB - dBarkRatio;
  dLeSDIB = dLeSD - dBarkRatio;

	if (dSDIB > 0.0 && dSDOB > 0.0)
	dBRatio = dSDIB/dSDOB;

	if((dLeSDIB > 0)&&(dLen > 0))
	{
		dTaperIB = (dLeSDIB - dSDIB)/dLen;
	}

	dTaperOB = dTaperIB/dBRatio;
	 
  dSEndDia = dSDOB;

	dSECSA = 0.002727*dSEndDia*dSEndDia;

	dX = dBoltLen;

	while(dX <= dLen)
	{
		dLEndDia = dSEndDia + dTaperOB*dBoltLen;
		dLECSA = 0.002727*dLEndDia*dLEndDia;

		dVolume += (dSECSA + dLECSA)*dBoltLen;

		dSEndDia = dLEndDia;
		dSECSA = dLECSA;

		dX += dBoltLen;
	}
	dX -= dBoltLen;

	if(dX < dLen)
	{
			dLEndDia = dSEndDia + dTaperOB*(dLen - dX);
			dLECSA = 0.002727*dLEndDia*dLEndDia;

			dVolume += (dSECSA + dLECSA)*(dLen - dX);
	}

    return dVolume;
}

// Simple function based on diamter and length/species
double CCalculationPreset::getUserVolFunc1(int spc_id,LPCTSTR table,double dSD1,double dSD2, double dLen)
{
	int nColumn = 1,nNumOfColumns = 0,nRow = 0;
	BOOL bSpeciesFound = FALSE,bDiamFound = FALSE,bLengthFound = FALSE;
	double fDiam = 0.0,fPrevValue = 0.0;
	CString sData(table),S;
	std::wstringstream ss;
	CString sToken = L"",sSpcID = L"",sLength1 = L"",sLength2 = L"",sDiam = L"",sValue  = L"";
	CStringArray sarrData1,sarrData2;
	std::wstring line;
	// Set average value of diameter
	if (dSD1 > 0.0 && dSD2 > 0.0)
	{
		fDiam = (dSD1+dSD2)/2.0;
	}
	else if (dSD1 > 0.0 && dSD2 == 0.0)
	{
		fDiam = dSD1;
	}
	else if (dSD2 > 0.0 && dSD1 == 0.0)
	{
		fDiam = dSD2;
	}

	ss << sData.GetBuffer();
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::SPECIES)
		{
			AfxExtractSubString(sSpcID,line.c_str(),1,';');
			bSpeciesFound = (_tstoi(sSpcID) == spc_id || _tstoi(sSpcID) == USER_CREATED_VOL_TABLE_SPC);
		}
		if (bSpeciesFound)
		{
			AfxExtractSubString(sToken,line.c_str(),0,';');
			//------------------------------------------------------------------------------------
			// Column header
			if (_tstoi(sToken) == FUNC_INDEX::COL_HEADER)
			{
				// Count number of columns
				while (AfxExtractSubString(sLength1,line.c_str(),nNumOfColumns+1,';')) 
					nNumOfColumns++;

				bLengthFound = FALSE;
				// Find column i.e. length
				for (nColumn = 1;nColumn < nNumOfColumns-1;nColumn++)
				{
					AfxExtractSubString(sLength1,line.c_str(),nColumn,';');

					tokenizeString(sLength1,'$',sarrData1);
					if (nColumn < nNumOfColumns-1)
					{
						AfxExtractSubString(sLength2,line.c_str(),nColumn+1,';');
						tokenizeString(sLength2,'$',sarrData2);
					}

					if (dLen >= _tstof(sarrData1[0]) && dLen < _tstof(sarrData2[0]))
					{
						bLengthFound = TRUE;
						break;
					}
					// If length is less than first length in table return 0.0
					if (dLen < _tstof(sarrData1[0]))
					{
						return 0.0;
					}
				}
			}
		}	// if (bSpeciesFound)
	} // while (std::getline(ss,line))

	if (!bLengthFound)
	{
		nColumn = nNumOfColumns-1;
	}

	ss.clear();
	ss << sData.GetBuffer();
	while (std::getline(ss,line))
	{
		AfxExtractSubString(sToken,line.c_str(),0,';');
		if (_tstoi(sToken) == FUNC_INDEX::SPECIES)
		{
			AfxExtractSubString(sSpcID,line.c_str(),1,';');
			bSpeciesFound = (_tstoi(sSpcID) == spc_id || _tstoi(sSpcID) == USER_CREATED_VOL_TABLE_SPC);
		}
		if (bSpeciesFound)
		{
			//------------------------------------------------------------------------------------
			AfxExtractSubString(sToken,line.c_str(),0,';');
			if (_tstoi(sToken) == FUNC_INDEX::DATA)
			{
				
				AfxExtractSubString(sDiam,line.c_str(),1,';');
				tokenizeString(sDiam,'$',sarrData1);
				AfxExtractSubString(sValue,line.c_str(),nColumn+1,';');
				if (fDiam < _tstof(sarrData1[0]))
				{
					bDiamFound = TRUE;
					break;
				}		

				fPrevValue = _tstof(sValue);
			}
			//------------------------------------------------------------------------------------		
		
		}	// if (bSpeciesFound)
	} // while (std::getline(ss,line))


	if (!bDiamFound)
	{
		fPrevValue = _tstof(sValue);
	}

	return fPrevValue;
}

//-------------------------------------------------------------------

double CCalculationPreset::GetLogValue(BYTE nVolumeIndex, double dVolume, double dPrice)
{
	double dValue = dVolume*dPrice;

	switch(nVolumeIndex)
	{
		case ID_INT14:
		case ID_INT18:
		case ID_DOYLE:
		case ID_BRUCE_SHUM:
		case ID_SCRIB_W:
//		case ID_JAS:
			dValue /= 1000.0;
			break;
		case ID_CFIB:
			dValue /= 100.0;
			break;
		case ID_CFIB_100:
			break;
		case ID_CFOB:
			dValue /= 2000.0;
			break;
	}

	return dValue;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PUBLIC METHODS

BOOL CCalculationPreset::calculateVolumeLogs(CLogs &rec,vecExtraVolFuncID &vec,CVecUserVolTables &vec1,double taper_ib,int use_southern_doyle,int measure_mode,int deduction,double trim)
{
	char s[10];
	int nLength = 0;
	double fIntFrac = 0.0;
	double fLength = 0.0;
	double fTrim = 0.0;
	double fVolume = 0.0;
	double fDeduction = 0.0;
	// Holds ALL userdefined volumefunctions
	m_vecUserVolTables = vec1;
	CLogs recLogs = rec;

	// Check if user has entered diamter and length
	if (recLogs.getUTopDia() > 0.0)
		recLogs.setDIBTop(recLogs.getUTopDia());

	if (recLogs.getUTopDia2() > 0.0)
		recLogs.setDIBTop2(recLogs.getUTopDia2());
	
	if (recLogs.getURootDia() > 0.0)
		recLogs.setDIBRoot(recLogs.getURootDia());

	if (recLogs.getURootDia2() > 0.0)
		recLogs.setDIBRoot2(recLogs.getURootDia2());

	//--------------------------------------------------------------

	if (recLogs.getUPrice() > 0.0)
	{
		recLogs.setLogPrice(rec.getUPrice());
		rec.setLogPrice(rec.getUPrice());
	}

	//--------------------------------------------------------------
	if (recLogs.getULength() > 0.0)
	{
		recLogs.setLengthMeas(recLogs.getULength());	
	}

	// Check Length for main volume
	if (recLogs.getVolFuncID() == ID_JAS && measure_mode == 0)
	{
			// Omvandla fr�n feet till meter
		fLength = recLogs.getLengthMeas()*FT2M;
		fTrim = trim*FT2M;	
		
		modf(fLength*10.0,&fIntFrac);
		sprintf(s,"%.0f",fIntFrac);
		nLength = atoi(s);
		if ((nLength % 2) == 1)
			nLength -= 1.0;

		fLength = ((nLength/10.0) - fTrim);		//#4103, fel variabel i formeln
		recLogs.setLengthCalc(fLength*M2FT);	//#3622, omvandlar tillbaks till feet
	}
	// Check Length for main volume
	else if (recLogs.getVolFuncID() == ID_JAS && measure_mode == 1)
	{
		fLength = recLogs.getLengthMeas();
		fTrim = trim/100.0;

		modf(fLength*10.0,&fIntFrac);
		sprintf(s,"%.0f",fIntFrac);
		nLength = atoi(s);
		if ((nLength % 2) == 1)
			nLength -= 1;

		fLength = ((nLength/10.0) - fTrim);
		recLogs.setLengthCalc(fLength);
	}
	else if (recLogs.getVolFuncID() != ID_JAS && measure_mode == 0)
	{
		recLogs.setLengthCalc(recLogs.getLengthMeas() - trim);
	}
	else if (recLogs.getVolFuncID() != ID_JAS && measure_mode == 1)
	{
		recLogs.setLengthCalc(recLogs.getLengthMeas() - trim/100.0);
	}


	// PERCENT
	if (deduction == 0)
		fDeduction = (100.0 - recLogs.getDeduction())/100.0;
	// ABSOLUTE
	else
		fDeduction = recLogs.getDeduction();

	// Calculate volume that price is based upon

	fVolume = GetLogVolume(recLogs.getVolFuncID(),recLogs,taper_ib,use_southern_doyle,measure_mode);
	if (fVolume > 0.0)
	{
		if (rec.getFrequency() > 0)
			fVolume *= rec.getFrequency();
		if (deduction == 0)
			fVolume = fVolume*fDeduction;
		else if (deduction == 1)
			fVolume = fVolume-fDeduction;
	}
	
	if (rec.getUVol() > 0.0)
		rec.setVolPricelist(recLogs.getUVol());
	else
		rec.setVolPricelist(fVolume);
	// Length used in calculation
	rec.setLengthCalc(recLogs.getLengthCalc());

	// Check if there's any extra volumefunctions to calcualte
	if (vec.size() > 0)
	{
		for (UINT i = 0;i < vec.size();i++)
		{
			// We need to look at lengths depending on type of function
			// and trim length acording to these roles.
			if (vec[i] != ID_JAS)
			{
				if (measure_mode == 0)
				{
					recLogs.setLengthCalc(recLogs.getLengthMeas() - trim);
				}
				else if (measure_mode == 1)
				{
					recLogs.setLengthCalc(recLogs.getLengthMeas() - trim/100.0);
				}

			}
			// Check Length for main volume
			else if (vec[i] == ID_JAS && measure_mode == 0)
			{
					// Omvandla fr�n feet till meter
				fLength = recLogs.getLengthMeas()*FT2M;
				fTrim = trim*FT2M;
				
				modf(fLength*10.0,&fIntFrac);
				sprintf(s,"%.0f",fIntFrac);
				nLength = atoi(s);
				if ((nLength % 2) == 1)
					nLength -= 1.0;

				fLength = ((nLength/10.0) - fTrim);		//#4103, fel variabel i formeln
				
				recLogs.setLengthCalc(fLength*M2FT);	//#3622, omvandlar tillbaks till feet

			}
			// Check Length for main volume
			else if (vec[i] == ID_JAS && measure_mode == 1)
			{
				fLength = recLogs.getLengthMeas();
				fTrim = trim/100.0;

				modf(fLength*10.0,&fIntFrac);
				sprintf(s,"%.0f",fIntFrac);
				nLength = atoi(s);
				if ((nLength % 2) == 1)
					nLength -= 1;

				fLength = ((nLength/10.0) - fTrim);		//#4103, fel variabel i formeln

				recLogs.setLengthCalc(fLength);

			}


			fVolume = GetLogVolume(vec[i],recLogs,taper_ib,use_southern_doyle,measure_mode);
			if (fVolume > 0.0)
			{
				if (rec.getFrequency() > 0)
					fVolume *= rec.getFrequency();
				if (deduction == 0)
					fVolume = fVolume*fDeduction;
				else if (deduction == 1)
					fVolume = fVolume-fDeduction;
			}
				
			switch (i)
			{
				case 0 : rec.setVolCust1(fVolume); break;
				case 1 : rec.setVolCust2(fVolume); break;
				case 2 : rec.setVolCust3(fVolume); break;
				case 3 : rec.setVolCust4(fVolume); break;
				case 4 : rec.setVolCust5(fVolume); break;
				case 5 : rec.setVolCust6(fVolume); break;
				case 6 : rec.setVolCust7(fVolume); break;
				case 7 : rec.setVolCust8(fVolume); break;
				case 8 : rec.setVolCust9(fVolume); break;
				case 9 : rec.setVolCust10(fVolume); break;
				case 10 : rec.setVolCust11(fVolume); break;
				case 11 : rec.setVolCust12(fVolume); break;
				case 12 : rec.setVolCust13(fVolume); break;
				case 13 : rec.setVolCust14(fVolume); break;
				case 14 : rec.setVolCust15(fVolume); break;
				case 15 : rec.setVolCust16(fVolume); break;
				case 16 : rec.setVolCust17(fVolume); break;
				case 17 : rec.setVolCust18(fVolume); break;
				case 18 : rec.setVolCust19(fVolume); break;
				case 19 : rec.setVolCust20(fVolume); break;
			};

		}
	}	// if (vec.size() > 0)

	return TRUE;
}
