#include "stdafx.h"
#include "Exports.h"
#include "volumefunctioncalculation.h"



//////////////////////////////////////////////////////////////////////////////////////////////////////
// EXPORTED FUNCTIONS

void getDLLVolFuncs(vecFuncDesc &vec)
{
	// Preset volumefunctions
	vec.push_back(CFuncDesc(ID_INT14,_T("International 1/4"),_T("Int-1/4"),_T("Int4-mbf"),1));
	vec.push_back(CFuncDesc(ID_INT18,_T("International 1/8"),_T("Int-1/8"),_T("Int8-mbf"),1));
	vec.push_back(CFuncDesc(ID_DOYLE,_T("Doyle"),_T("Doyle"),_T("Doyle-mbf"),1));
	vec.push_back(CFuncDesc(ID_BRUCE_SHUM,_T("Bruce/Shumacher"),_T("BruceShum"),_T("BruceShum-mbf"),1));
	vec.push_back(CFuncDesc(ID_SCRIB_W,_T("Scribner West-Side"),_T("ScribWS"),_T("ScribWS-mbf"),1));
	vec.push_back(CFuncDesc(ID_JAS,_T("Japanese Agricultural Std."),_T("JAS"),_T("JAS-m3"),1));
	vec.push_back(CFuncDesc(ID_HUBER,_T("Huber"),_T("Huber"),_T("HUB-m3"),1));
	// Commented out 2012-05-28 P�D
	//vec.push_back(CFuncDesc(ID_HUBER_TR,_T("Huber-Turkey"),_T("Huber-TR"),_T("HUB-TR-m3"),1));
	vec.push_back(CFuncDesc(ID_GB_SCALE,_T("GB scale (Guo Biao)"),_T("GB scale"),_T("GBScale-m3"),1));
	vec.push_back(CFuncDesc(ID_CFIB,_T("Cubic-feet inside bark"),_T("Cubic-feet"),_T("Cunit"),0));
	vec.push_back(CFuncDesc(ID_CFIB_100,_T("Cubic-feet inside bark, piece"),_T("Piece"),_T("Piece"),0));
	vec.push_back(CFuncDesc(ID_CFOB,_T("Cubic-feet on bark"),_T("Pound"),_T("Ton"),0));
}

void calculateDLLVolLogs(CLogs &rec,vecExtraVolFuncID &vec,CVecUserVolTables &vec1,double taper_ib,int use_southern_doyle,int measure_mode,int deduction,double trim)
{
	CCalculationPreset *pCalc = new CCalculationPreset();
	if (pCalc != NULL)
	{
		pCalc->calculateVolumeLogs(rec,vec,vec1,taper_ib,use_southern_doyle,measure_mode,deduction,trim);
		delete pCalc;
	}
}