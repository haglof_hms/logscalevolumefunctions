#if !defined(AFX_VOLUMEFUNCTIONCALCULATION_H)
#define AFX_VOLUMEFUNCTIONCALCULATION_H

#include <sstream>

class segment_values
{
	double length;
	double add_to_diam;
public:
	segment_values()
	{
		length = 0.0;
		add_to_diam = 0.0;
	}

	segment_values(double l,double d)
	{
		length = l;
		add_to_diam = d;
	}

	double getLength()	{ return length; }
	double getAddToDiam()	{ return add_to_diam; }


};

typedef std::vector<segment_values> vecSegmentValues;

class CCalculationBase
{
public:
	CCalculationBase();
};


class CCalculationPreset : public CCalculationBase
{
	typedef enum { SIMPLE,  NO_FRACTIONS, ROUND_NO_FRACTIONS, HUBER_TR, GB_SCALE, DO_NOTHING } enumScalingDiamTypes;

	typedef enum { CONV_JAS,CONV_HIRAGOKU, CONV_SSL } enumConvertType;

	CVecUserVolTables m_vecUserVolTables;

	double m_fOmfNum;

//	double GetLogVolume(BYTE nVolumeIndex,double dSD1,double dSD2, double dLen, double dTaperIB, double dBarkRatio, WORD wUseSouthernDoyle, double dLeSD1 = 0.0, double dLeSD2 = 0.0, int nMeasureSys = 0);
	double GetLogVolume(int nVolumeIndex,CLogs &rec,double dTaperIB,WORD wUseSouthernDoyle,int nMeasureSys);

	double GetDoyle(double dSD, double dLen, WORD wUseSouthernDoyle);
	double GetKnauf(double dSD, double dLen);
	double GetBruceShum(double dSD, double dLen);
	double GetScribnerW(double dSD, double dLen);
	double GetScribnerE(double dSD, double dLen);
	double GetJAS(double dSD, double dLen);
	double GetHuber(double dSD, double dLen);
	double GetHuberTR(double dSD, double dLen);
	double GetGBScale(double dSD, double dLen);
	double GetScrib16FootSegm(double dSD,double dTaperIB);
	double GetDoyle16FootSegm(double dSD,double dTaperIB);
	double GetInt8(double dSD, double dLen, double dTaperIB, double dLeSD = 0.0);
	double GetCfib(double dSDIB, double dLen, double dTaperIB, double dLeSD = 0.0);
	double GetCfob(double dSDOB, double dLen, double dTaperIB, double dBarkRatio, double dLeSD = 0.0);
	double GetLogValue(BYTE nVolumeIndex, double dVolume, double dPrice);

	// Userdefined volumefunctions
	double getUserVolFunc1(int spc_id,LPCTSTR table,double dSD1,double dSD2, double dLen);

	void segment(double length,vecSegmentValues& values);

	// Misc. methods
	double getScalingDiam(double d1,double d2,enumScalingDiamTypes scaling_type);
	double getAbs(double v);
	double getHuberTRLength(double len);
	double getGBScaleLength(double len);

	double addToJAS(double dS,double dL);
	double checkDiameter(enumConvertType convert_type,double dS,double dL);
	
	void tokenizeString(LPCTSTR str,TCHAR c,CStringArray& items);


public:
	CCalculationPreset();

	BOOL calculateVolumeLogs(CLogs &rec,vecExtraVolFuncID &vec,CVecUserVolTables &vec1,double taper_ib,int use_southern_doyle,int measure_mode,int deduction,double trim);
};


#endif